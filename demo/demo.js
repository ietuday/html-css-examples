let name = "John";

// embed a variable
console.log( `Hello, ${name}!` ); // Hello, John!

// embed an expression
console.log( `the result is ${1 + 2}` ); // the result is 3

console.log(
  typeof undefined,
  typeof 0,
  typeof true,
  typeof "foo",
  typeof Symbol("id"),
  typeof Math,
  typeof null,
  typeof alert
);

let name1 = "Ilya";

// the expression is a number 1
console.log( `hello ${1}` ); // hello 1

// the expression is a string "name"
console.log( `hello ${"name"}` ); // hello name

// the expression is a variable, embed it
console.log( `hello ${name1}` ); // hello Ilya

let value1 = true;

console.log("Type of value1 : ",typeof value1);

value1 = String(value1);

console.log("After Explicit Conversion : ",typeof value1);

let str1 = "123";

console.log("Type of str1",typeof str1);

let num = Number(str1);

console.log("Type od num: ",typeof num);

let age = Number("an arbitrary string instead of a number");

console.log(age); // NaN, conversion failed

console.log( Number("   123   ") ); // 123
console.log( Number("123z") );      // NaN (error reading a number at "z")
console.log( Number(true) );        // 1
console.log( Number(false) );       // 0


console.log( Boolean(1) ); // true
console.log( Boolean(0) ); // false

console.log( Boolean("hello") ); // true
console.log( Boolean("") ); // false

console.log( Boolean("0") ); // true
console.log( Boolean(" ") ); // spaces, also true (any non-empty string is true)

/**
 *
 ToString – Occurs when we output something, can be performed with String(value). The conversion to string is usually obvious for primitive values.

ToNumber – Occurs in math operations, can be performed with Number(value).

The conversion follows the rules:

Value	Becomes…
undefined	NaN
null	0
true / false	1 / 0
string	The string is read “as is”, whitespaces from both sides are ignored. An empty string becomes 0. An error gives NaN.
ToBoolean – Occurs in logical operations, or can be performed with Boolean(value).

Follows the rules:

Value	Becomes…
0, null, undefined, NaN, ""	false
any other value	true
Most of these rules are easy to understand and memorize. The notable exceptions where people usually make mistakes are:

undefined is NaN as a number, not 0.
"0" and space-only strings like " " are true as a boolean.
 */

console.log("Result : ","" + 1 + 0); // (1)
console.log("Result: ","" - 1 + 0);
console.log("Result: ",true + false);
console.log("Result: ",6 / "3");
console.log("Result : ","2" * "3");
console.log("Result : ",4 + 5 + "px");
console.log("Result : ","$" + 4 + 5);
console.log("Result : ","4" - 2);
console.log("Result : ","4px" - 2);
console.log("Result : ",7 / 0);
console.log("Result : "," -9\n" + 5);
console.log("Result : ", " -9\n" - 5);
console.log("Result : ",null + 1);
console.log("Result : ",undefined + 1);


/**
 * Trim Method in javascript
 */

var orig = '   foo  ';
console.log("After Applying trim method",orig.trim()); // 'foo'

// Another example of .trim() removing whitespace from just one side.

var orig = 'foo    ';
console.log("After Applying trim method",orig.trim()); // 'foo'


if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

var string = "  Hello world";
console.debug(string.trimLeft());
console.log(string.trimLeft());


var someObject = { str: "Some text", id: 5 };
console.log(someObject);

var car = "Dodge Charger";
var someObject = { str: "Some text", id: 5 };
console.info("My first car was a", car, ". The object is:", someObject);

for (var i=0; i<5; i++) {
  console.log("Hello, %s. You've called me %d times.", "Bob", i+1);
}

console.log("This is %cMy stylish message", "color: yellow; font-style: italic; background-color: blue;padding: 2px");

/**
 * Console Methods
 */

console.log("This is the outer level");
console.group();
console.log("Level 2");
console.group();
console.log("Level 3");
console.warn("More of level 3");
console.groupEnd();
console.log("Back to level 2");
console.groupEnd();
console.debug("Back to the outer level");

console.time("answer time1");
setTimeout(() => {
  console.timeEnd("answer time2");
}, 2000);


function foo() {
  function bar() {
    console.trace();
  }
  bar();
}

foo();


let age1 = prompt('How old are you?', 100);

console.log(`You are ${age1} years old!`); // You are 100 years old!


let isBoss = confirm("Are you the boss?");

console.log( isBoss ); // true if OK is pressed

/**
 alert
shows a message.
prompt
shows a message asking the user to input text. It returns the text or, if CANCEL or Esc is clicked, all browsers return null.
confirm
shows a message and waits for the user to press “OK” or “CANCEL”. It returns true for OK and false for CANCEL/Esc.
All these methods are modal: they pause the script execution and don’t allow the visitor to interact with the rest of the page until the message has been dismissed.

There are two limitations shared by all the methods above:

The exact location of the modal window is determined by the browser. Usually it’s in the center.
The exact look of the window also depends on the browser. We can’t modify it.

 */

let age2 = prompt('age?', 18);

let message = (age2 < 3) ? 'Hi, baby!' :
  (age2 < 18) ? 'Hello!' :
  (age2 < 100) ? 'Greetings!' :
  'What an unusual age!';

console.log("Message :",message);


let value = prompt('What is the "official" name of JavaScript?', '');

if (value == 'ECMAScript') {
  console.log('Right!');
} else {
  console.log("You don't know? ECMAScript!");
}


    let userName = prompt("Who's there?", '');

    if (userName == 'Admin') {

      let pass = prompt('Password?', '');

      if (pass == 'TheMaster') {
          console.log( 'Welcome!' );
      } else if (pass == null) {
        console.log( 'Canceled.' );
      } else {
        console.log( 'Wrong password' );
      }

    } else if (userName == null) {
      console.log( 'Canceled' );
    } else {
      console.log( "I don't know you" );
    }

let user = new Object();
user = {     // an object
  name: "John",  // by key "name" store value "John"
  age: 30        // by key "age" store value 30
};
console.log("User Object : ",user);
// get fields of the object:
console.log( user.name ); // John
console.log( user.age ); // 30
user.isAdmin = true;
console.log(user);

delete user.age;
console.log(user);

user = {
  name: "John",
  age: 30,
  "likes birds": true  // multiword property name must be quoted
};

// set
user["likes birds"] = true;

// get
console.log(user["likes birds"]); // true

// delete
delete user["likes birds"];;


let key = "likes birds";

// same as user["likes birds"] = true;
user[key] = true;

console.log(user);

key = prompt("What do you want to know about the user?", "name");

// access by variable
console.log( user[key] ); // John (if enter "name")

console.log(user);

let fruit = prompt("Which fruit to buy?", "apple");

let bag = {
  [fruit]: 5, // the name of the property is taken from the variable fruit
};

console.log(bag.apple); // 5 if fruit="apple"

function makeUser(name, age) {
  return {
    name: name,
    age: age
    // ...other properties
  };
}

user = makeUser("John", 30);
console.log(user.name); // John


user = {};

console.log( user.noSuchProperty === undefined ); // true means "no such property"

user = { name: "John", age: 30 };

console.log( "age" in user ); // true, user.age exists
console.log( "blabla" in user ); // false, user.blabla doesn't exist


user = {
  name: "John",
  age: 30,
  isAdmin: true
};

for(let key in user) {
  // keys
  console.log("key : ",key);  // name, age, isAdmin
  // values for the keys
  console.log( "Value : ",user[key]); // John, 30, true
}

let codes = {
  "49": "Germany",
  "41": "Switzerland",
  "44": "Great Britain",
  // ..,
  "1": "USA"
};

for(let code in codes) {
  console.log(code); // 1, 41, 44, 49
  console.log(codes[code]); // 1, 41, 44, 49

}


console.log( String(Math.trunc(Number("49"))) ); // "49", same, integer property
console.log( String(Math.trunc(Number("+49"))) ); // "49", not same "+49" ⇒ not integer property
console.log( String(Math.trunc(Number("1.2"))) ); // "1", not same "1.2" ⇒ not integer property


user = {
  name: "John",
  surname: "Smith"
};
user.age = 25; // add one more

// non-integer properties are listed in the creation order
for (let prop in user) {
  console.log( prop ); // name, surname, age
}


codes = {
  "+49": "Germany",
  "+41": "Switzerland",
  "+44": "Great Britain",
  // ..,
  "+1": "USA"
};

for(let code in codes) {
  console.log( +code ); // 49, 41, 44, 1
}

let	myStr	=	'I	think,	therefore	I	am.'
if	(myStr.indexOf('think')	!=	-1){
		console.log	(myStr);
}

let username	= 'Brad';
let	output	=	'<username>	please	enter	your	password: ';
output.replace('<username>',	username);
console.log("output : ",output);


let	t	=	'12:10:36';
let	tArr	=	t.split(':');
let	hour	=	tArr[0];
let	minute	=	tArr[1];
let	second	=	tArr[2];

console.log(t);
console.log(tArr);
console.log(hour);
console.log(minute);
console.log(second);
